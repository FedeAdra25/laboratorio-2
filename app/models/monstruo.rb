class Monstruo < ApplicationRecord
	validates :nombre,presence: true,uniqueness: true
	has_many:victimas,dependent: :destroy

end