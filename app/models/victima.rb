class Victima < ApplicationRecord
	belongs_to:monstruo
	default_scope 		-> {order(nombre: :asc)}
	scope :mayores, 	-> (edad_min) {
		where("edad > ?",edad_min)
	}


end
